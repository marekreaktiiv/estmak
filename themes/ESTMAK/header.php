<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>
		<?php $header_logo = get_field('header_logo', 'option'); ?>
		<?php $header_text = get_field('header_text', 'option'); ?>
		<?php $header_background = get_field('header_background', 'option'); ?>

		<header class="header clear" role="banner" style="background-image: url('<?php echo $header_background; ?>')">

			<button type="button" class="mobile-menu-btn">
				<span></span>
				<span></span>
				<span></span>
			</button>

			<div id="topnav">
				<nav id="topnav_left">
					<?php wp_nav_menu(array('menu' => 'Top Menu Left')); ?>
				</nav>
				<nav id="topnav_right">
					<?php wp_nav_menu(array('menu' => 'Top Menu right')); ?>
				</nav>
			</div>

			<nav id="sidenav">
				<?php wp_nav_menu(array('menu' => 'Side Menu')); ?>
			</nav>

			<div class="logo">
				<span class="logo_helper"></span>
				<a href="<?php echo home_url(); ?>">
					<img src="<?php echo $header_logo; ?>" alt="Logo" class="logo-img">
				</a>
			</div>

			<h1><?php echo $header_text; ?></h1>

			<div class="scroll_down_arrow">
				<img src="<?php echo get_template_directory_uri(); ?>/img/alla_nool.png" alt="scroll" >
			</div>
		</header>
