		<?php $footer_logo = get_field('footer_logo', 'option'); ?>
		<?php $footer_company = get_field('footer_company', 'option'); ?>
		<?php $footer_address = get_field('footer_address', 'option'); ?>
		<?php $footer_email = get_field('footer_email', 'option'); ?>

		<footer id="contact">
			<img src="<?php echo $footer_logo; ?>" alt="Footer_logo">

			<div class="footer_info">
				<span><?php echo $footer_company; ?></span>
				<span><?php echo $footer_address; ?></span>
				<span><a href="mailto:<?php echo $footer_email; ?>"><?php echo $footer_email; ?></a></span>
			</div>
		</footer>

		<?php wp_footer(); ?>
	</body>
</html>
