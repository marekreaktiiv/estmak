<?php
if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true);
    add_image_size('medium', 250, '', true);
    add_image_size('small', 120, '', true);
    add_image_size('custom-size', 700, 200, true);

    add_theme_support('automatic-feed-links');
}

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'ESTMAK Settings',
		'menu_title'	=> 'ESTMAK Settings',
		'menu_slug'	=> 'estmak-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

// ESTMAKnavigation
function ESTMAK_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

// Load styles
function ESTMAK_styles()
{
    wp_register_style('ESTMAK', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('ESTMAK');

    wp_register_style('ESTMAK_mobile', get_template_directory_uri() . '/css/mobile.css', array(), '1.0', 'all');
    wp_enqueue_style('ESTMAK_mobile');

    wp_register_style('lightgallery', get_template_directory_uri() . '/css/lightgallery.min.css', array(), '1.0', 'all');
    wp_enqueue_style('lightgallery');
}


function ESTMAK_header_scripts()
{
   if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
       wp_register_script('ESTMAK', get_template_directory_uri() . '/js/script.js', array('jquery'), '1.0.0');
       wp_enqueue_script('ESTMAK');
   }
}

function ESTMAK_footer_scripts()
{
    wp_register_script('lightgallery', get_template_directory_uri() . '/js/lib/lightgallery-all.min.js', array('jquery'), '1.6.10');
    wp_enqueue_script('lightgallery');
}

// Register ESTMAK Navigation
function register_ESTMAK_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'ESTMAK'), // Main Navigation
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}


// Add Actions
add_action('get_header', 'enable_threaded_comments');
add_action('wp_enqueue_scripts', 'ESTMAK_styles');
add_action('init', 'ESTMAK_header_scripts');
add_action('wp_print_scripts', 'ESTMAK_footer_scripts');

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('body_class', 'add_slug_to_body_class');
add_filter('widget_text', 'do_shortcode');
add_filter('widget_text', 'shortcode_unautop');
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args');
add_filter('the_category', 'remove_category_rel_from_category_list');
add_filter('the_excerpt', 'shortcode_unautop');
add_filter('the_excerpt', 'do_shortcode');
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10);
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10);

// Remove Filters
remove_filter('the_excerpt', 'wpautop');
?>
