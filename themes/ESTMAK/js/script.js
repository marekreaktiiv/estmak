(function ($, root, undefined) {
	$(function () {
		'use strict';

		$(document).ready(function () {
			checkWindowSize();

			var windowWidth = $(window).width();
			var headerMenuHeight = $('header .logo').height();

			if (windowWidth > 780) {
				var emptyRoomTop = headerMenuHeight;
			} else {
				var emptyRoomTop = 0;
			}

			$('.mobile-menu-btn').on('click', function () {
				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
					$('#topnav').removeClass('active');
				} else {
					$(this).addClass('active');
					$('#topnav').addClass('active');
				}

			});

			$('.scroll_down_arrow img').on('click', function () {
				var targetOffset = $('section').offset().top;
				var scrollFromTop = targetOffset - emptyRoomTop;
				$('html,body').animate({
					scrollTop: scrollFromTop
				}, 1000);
			});

			$(".scroll_down_arrow img").mouseover(function () {
				$(".scroll_down_arrow").addClass('hovered');
			}).mouseout(function () {
				$(".scroll_down_arrow").removeClass('hovered');
			});


			// Create galleries
			var galleries = document.querySelectorAll('*[id^="gallery-"]');
			for (var i = galleries.length; i--;) {
				var galleryID = String($(galleries[i]).attr('id'));
				$("#" + galleryID).lightGallery({
					download: false,
					share: false,
					selector: '.single_gallery_image'
				});
			}

			$('.dev_gallery').on('click touchend', function (e) {
				var openGallery = $(this).attr('data-gallery');
				$("#gallery-" + openGallery + " div:first-child").click();
			});

			// Scroll
			$('a[href*="#"]').not('[href="#"]').not('[href="#0"]').click(function (event) {
				if (
					location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
					&&
					location.hostname == this.hostname
				) {
					var target = $(this.hash);
					target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

					if (target.length) {
						event.preventDefault();

						if ($('.mobile-menu-btn').hasClass('active')) {
							$('.mobile-menu-btn').removeClass('active');
							$('#topnav').removeClass('active');
						}

						var targetOffset = target.offset().top;
						var scrollFromTop = targetOffset - emptyRoomTop;
						$('html,body').animate({
							scrollTop: scrollFromTop
						}, 0);
					}
				}
			});
		});

		$(window).scroll(function () {
			checkWindowSize();
		});

		$(window).resize(function () {
			checkWindowSize();
		});

		function checkWindowSize() {
			var windowWidth = $(window).width();
			var windowHeight = $(window).height();

			if (windowWidth >= 1250 && windowHeight <= 700) {
				if ($('header').hasClass('scroll') == false) {
					$('header').addClass('scroll');
				}
			} else if (windowWidth <= 1250 && windowWidth >= 780 && windowHeight <= 650) {
				if ($('header').hasClass('scroll') == false) {
					$('header').addClass('scroll');
				}
			} else if (windowWidth <= 780) {
				if ($('header').hasClass('scroll')) {
					$('header').removeClass('scroll');
				}
			} else {
				menuMovement();
			}
		}

		function menuMovement() {
			var doc = document.documentElement;
			var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

			if (top <= 10) {
				if ($('header').hasClass('scroll')) {
					$('header').removeClass('scroll');
				}
			} else {
				if ($('header').hasClass('scroll') == false) {
					$('header').addClass('scroll');
				}
			}
		}
	});
})(jQuery, this);
