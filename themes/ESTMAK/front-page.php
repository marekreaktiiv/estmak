<?php get_header(); ?>
    <section>
        <div id="our_story">
            <?php $our_story_text = get_field('our_story_text'); ?>
            <?php $our_story_title = get_field('our_story_title'); ?>

            <div class="our_story_title">
                <h3><?php echo $our_story_title; ?></h3>
            </div>

            <div class="our_story_logo">
                <span class="logo_helper"></span>
                <img src="<?php echo get_template_directory_uri(); ?>/img/our_story_logo.png">
            </div>

            <?php if($our_story_text): ?>
                <p><?php echo $our_story_text; ?></p>
            <?php endif; ?>
        </div>

        <?php $counter = 0; ?>
        <?php if( have_rows('projects') ): ?>
            <div id="projects">
                <h1><?php the_field('projects_title'); ?></h1>

                <div class="projects_container">
                    <?php while ( have_rows('projects') ) : the_row(); ?>
                        <?php $galleryCounter = 0; ?>
                        <?php $project_title = get_sub_field('project_title'); ?>
                        <?php $project_gallery = get_sub_field('project_gallery'); ?>
                        <?php $project_image = get_sub_field('project_image'); ?>
                        <?php $project_address = get_sub_field('project_address'); ?>
                        <?php $project_link = get_sub_field('project_link'); ?>

                        <div class="single_project">
                            <div class="project_title_area">
                                <h2><?php echo $project_title; ?></h2>
                                <span>
                                    <?php if($project_gallery): ?>
                                        <a class="dev_gallery" data-gallery="<?php echo $counter; ?>">gallery</a>
                                    <?php else: ?>
                                        gallery
                                    <?php endif; ?>
                                </span>
                            </div>

                            <?php if($project_gallery): ?>
                                <div id="gallery-<?php echo $counter; ?>" class="hidden">
                                    <?php foreach ($project_gallery as $image) : ?>
                                        <div class="single_gallery_image" id="gallery_item-<?php echo $counter; ?>_<?php echo $galleryCounter; ?>" data-src="<?php echo $image['url']; ?>">
                                            <img src="<?php echo $image['url']; ?>" />
                                        </div>
                                        <?php $galleryCounter++; ?>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>

                            <img src="<?php echo $project_image; ?>">

                            <div class="project_info">
                                <span><?php echo $project_address; ?></span>
                                <span><a href="http://<?php echo $project_link; ?>" target="_blank"><?php echo $project_link; ?></a></span>
                            </div>
                        </div>

                        <?php $counter++; ?>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif ?>

        <?php if( have_rows('developments') ): ?>
            <div id="developments">
                <h1><?php the_field('developments_title'); ?></h1>

                <div class="projects_container">
                    <?php while ( have_rows('developments') ) : the_row(); ?>
                        <?php $project_title = get_sub_field('project_title'); ?>
                        <?php $project_gallery = get_sub_field('project_gallery'); ?>
                        <?php $project_image = get_sub_field('project_image'); ?>
                        <?php $project_address = get_sub_field('project_address'); ?>
                        <?php $project_link = get_sub_field('project_link'); ?>

                        <div class="single_project">
                            <div class="project_title_area">
                                <h2><?php echo $project_title; ?></h2>
                                <span>
                                    <?php if($project_gallery): ?>
                                        <a class="dev_gallery" data-gallery="<?php echo $counter; ?>">gallery</a>
                                    <?php else: ?>
                                        gallery
                                    <?php endif; ?>
                                </span>
                            </div>

                            <?php if($project_gallery): ?>
                                <div id="gallery-<?php echo $counter; ?>" class="hidden">
                                    <?php foreach ($project_gallery as $image) : ?>
                                        <div class="single_gallery_image" id="gallery_item-<?php echo $counter; ?>_<?php echo $galleryCounter; ?>" data-src="<?php echo $image['url']; ?>">
                                            <img src="<?php echo $image['url']; ?>" />
                                        </div>
                                        <?php $galleryCounter++; ?>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>

                            <img src="<?php echo $project_image; ?>">

                            <div class="project_info">
                                <span><?php echo $project_address; ?></span>
                                <span><a href="http://<?php echo $project_link; ?>" target="_blank"><?php echo $project_link; ?></a></span>
                            </div>
                        </div>
                        <?php $counter++; ?>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif ?>
    </section>
<?php get_footer(); ?>
